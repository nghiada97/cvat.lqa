import unittest
from DownloadFile.Base.Base import Base
from DownloadFile.Page.LoginPage import Login
from DownloadFile.Page.Tasks import Tasks
import time


class Login1(unittest.TestCase):

    base = Base()

    @classmethod
    def setUpClass(cls, base=base):

        base.setUpClassChrome()
        base.get('http://cvat.lqa.com.vn/auth/login/?next=/tasks')


    def test_Download(self):
        driver = self.base

        login1 = Login(driver)
        login1.login()

        task = Tasks(driver)
        task.readFileEx()
        task.downloadFile1()



    def tearDown(cls, base = base):
        time.sleep(10)
        base.quit()