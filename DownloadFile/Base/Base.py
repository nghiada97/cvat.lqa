from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select


class Base():

    def __init__(self):
        pass

    def setUpClassChrome(self):
        self.driver = webdriver.Chrome(executable_path='D:/New folder/Nghia/chromedriver.exe')
        self.driver.implicitly_wait(10)
        # self.driver.maximize_window()

    def get(self, param: str):
        self.driver.get(param)

    def quit(self):
        self.driver.quit()

    def Click(self, by:str):
        try:
            self.driver.find_element_by_xpath(by).click()
        except:
            print('Dont Click: '+ by)
            raise Exception

    def SendKeys(self, by:str, Str: str):
        try:
            # self.ExplicitWait(by)
            self.driver.find_element_by_xpath(by).clear()
            self.driver.find_element_by_xpath(by).send_keys(Str)
        except :
            print('Dont sendkey: '+ by)
            raise Exception

    def Hover(self, by:str):
        try:
            action = ActionChains(self.driver)
            element = self.driver.find_element_by_xpath(by)
            action.move_to_element(element).perform()
        except:
            print('Dont hover: ' + by)
            raise Exception

    def getText(self, by: str):
        try:
            return self.driver.find_element_by_xpath(by).text
        except:
            print('Dont get text: ' + by)
            raise Exception

    def back(self):
        self.driver.back()

    def clear(self, by):
        self.driver.find_element_by_xpath(by).clear()

