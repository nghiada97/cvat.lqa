from DownloadFile.Base.Base import Base
import time
import xlrd


class Tasks:
    base = Base()

    def __init__(self, driver):
        self.base = driver

        self.NextPage = '//li[@title="Next Page"]//button[@class="ant-pagination-item-link"]'
        self.ActionB = '//button[@type="button"]//span[@class="ant-typography cvat-text-color"]'
        self.CVATimage1 = '//div[contains(@style, "position: absolute")][2]//li[3]/span[1]'
        self.search = '//input[@placeholder="Search"]'
        self.searchButton = '//button[@class="ant-btn ant-btn-lg ant-btn-icon-only ant-input-search-button"]'

    def Actionsbutton(self, i:int):
        return "//div[contains(@class, 'tasks-list-item')]["+str(i)+"] //span[contains(@class , 'anticon ant-dropdown')]"


    def Name(self, i: int):
        return "//div[contains(@class, 'tasks-list-item')]["+str(i)+"] //span[contains(@class , 'cvat-item-task-name')]"

    def Open(self, i: int):
        return "//div[contains(@class, 'tasks-list-item')]["+str(i)+"]//div[@class='ant-col']/a"

    def Dump(self, i:int):
        return "//div[contains(@style, 'position: absolute')]["+str(i)+"]//li[1]//div[@class ='ant-menu-submenu-title']"

    def CVATimage(self, i:int):
        return " //div[contains(@style, 'position: absolute')]["+str(i)+"]//li[1]//span[@class='anticon anticon-download']"

    # list = [ 'Task_23_[09022021]_[L1]_[F00025]_[2]_[100]',
    #          'Task_030_[01032021]_[L2]_[F00003]_[17]_[100]',
    #          'Task_029_[01032021]_[L2]_[F00002]_[28]_[100]'
    # ]

    list = []

    def readFileEx(self):
        loc = ('/New folder/pythonProject1/test.xlsx')

        wb = xlrd.open_workbook(loc)
        sheet = wb.sheet_by_index(0)
        sheet.cell_value(0,0)
        for i in range(sheet.nrows):
            self.list.append(sheet.cell_value(i,0))

    def downloadFile1(self):
        for i in self.list:
            self.base.SendKeys(self.search, i)
            self.base.Click(self.searchButton)
            # self.base.Click(self.Open(1))
            self.base.Hover(self.Actionsbutton(1))
            time.sleep(0.2)
            self.base.Hover(self.Dump(1))
            time.sleep(0.2)
            self.base.Click(self.CVATimage1)
            self.base.back()


